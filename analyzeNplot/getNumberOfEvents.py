import math, ROOT 
import argparse
import os, sys

#configuration
parser = argparse.ArgumentParser()
parser.add_argument('-i', '--inDir',  help='input directory with files',   default=None,    type=str)
parser.add_argument('-o', '--outDir', help='output directory',             default='table', type=str)
args = parser.parse_args()

#prepare output
texFile = "NumberOfEvents.tex"
tex = open(os.path.join(args.outDir,texFile), 'w')
tex.write("\\documentclass[a4paper]{article}\n")
tex.write("\\usepackage{array}\n")
tex.write("\\usepackage{amsmath}\n")
tex.write("\\newcommand{\\ttbar}{\\ensuremath{\\text{t}\\bar{\\text{t}}}}\n")
tex.write("\\begin{document}\n")
tex.write("\\begin{center}\n")
tex.write("\n%%%\n")

tex.write("\\begin{table}[b!] \n")
tex.write("\\begin{center} \n")
tex.write("\\begin{tabular}{lc} \n")
tex.write("Process & Number of events \\\\ \n")
tex.write("\\hline\n")

if not os.path.isdir(args.outDir):
    os.mkdir(args.outDir)

inF = ROOT.TFile.Open(os.path.join(os.path.join(args.inDir,"plots"),"plotter.root"))

#iterate over all processes
NSim = 0.
ErrNSim = 0.
inD = inF.Get("nvtx")
for tkey in inD.GetListOfKeys():
    key=tkey.GetName()
    hist=inD.Get(key)
    if not hist.InheritsFrom('TH1'): 
        continue
    name = hist.GetTitle()
    if name == "Data":
        continue
    NTot = 0.
    ErrNTot = 0.
    for i in range(0,hist.GetNbinsX()+2):
        NTot += hist.GetBinContent(i)
        ErrNTot += hist.GetBinError(i)*hist.GetBinError(i)
    #NTot = hist.Integral()
    ErrNTot = ROOT.TMath.Sqrt(ErrNTot)
    NSim += NTot
    ErrNSim += ErrNTot*ErrNTot
    if ErrNTot > 1:
        tex.write("%s & $%d\\pm%d$ \\\\ \n" % (name.replace("t#bar{t}","\\ttbar"), NTot, ErrNTot))
    else:
        tex.write("%s & $%.1f\\pm%.1f$ \\\\ \n" % (name.replace("t#bar{t}","\\ttbar"), NTot, ErrNTot))
 
hist = inF.Get("nvtx/nvtx").Clone()
NData = hist.Integral(-1,-1)

# finish output
tex.write("\\hline\n")
tex.write("Total from simulations & $%d\\pm%d$ \\\\ \n" % (NSim, ErrNTot))
tex.write("Data & $%d$ \\\\ \n" % (NData))
        
tex.write("\\end{tabular}\n")
tex.write("\\end{center}\n")
tex.write("\\end{table}\n")

tex.write("\n%%%\n")
tex.write("\\end{center}\n")
tex.write("\n\\end{document}")
    
tex.close()

savedir = os.getcwd()
os.chdir(args.outDir)
cmd = "pdflatex " + texFile
os.system(cmd)
cmd = "rm -f *.aux *.log"
os.system(cmd)
#os.system("cd -")
os.chdir(savedir)
print("\n"+os.path.join(args.outDir,texFile)+" has been created and compiled.")
