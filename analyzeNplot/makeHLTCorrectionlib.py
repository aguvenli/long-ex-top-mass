import argparse
import os, sys
import uproot
import gzip
import numpy as np

import correctionlib
import correctionlib.convert
import rich

def makeHLTCorrecionlib(input_filename):
    """
    provided root TH2 histograms are converted via correctionlib, only needs to be done once
    """
    
    print(f"Convert {input_filename} to correctionlib compatible json")
    with uproot.open(input_filename) as tFile:
        sfhist = tFile["h2D_SF_emu_lepABpt_FullErrorstat"].to_hist()

    # without a name, the resulting object will fail validation
    sfhist.name = "h2D_emu_lepABpt_SF"
    sfhist.label = "HLT Scale Factors"
    corr_HLT = correctionlib.convert.from_histogram(sfhist)
    corr_HLT.description = "HLT scale factors in 2D in bins of pt of the muon and electron"
    # set overflow bins behavior (default is to raise an error when out of bounds)
    corr_HLT.data.flow = "clamp"
    corr_HLT.output.description = "Multiplicative event weight"

    # The uncertainty is stored in the histogram uncertainty
    sfhist_err = sfhist.copy()
    sfhist_err.values(flow=True)[...] += np.sqrt(sfhist.variances(flow=True)[...])
    sfhist_err.name = "h2D_emu_lepABpt_SFup"
    sfhist_err.label = "HLT Scale Factors"
    corr_HLT_err = correctionlib.convert.from_histogram(sfhist_err)
    corr_HLT_err.description = "HLT scale factors variation up in 2D in bins of pt of the muon and electron"
    corr_HLT_err.data.flow = "clamp"
    corr_HLT_err.output.description = "Multiplicative event weight"

    # print some information about the correction
    rich.print(corr_HLT)
    rich.print(corr_HLT_err)

    cset = correctionlib.schemav2.CorrectionSet(
        schema_version=2,
        description="HLT custom scale factors measured for TOP-23-008",
        corrections=[
            corr_HLT,
            corr_HLT_err,
        ],
    )
    # save converted correction
    with gzip.open(input_filename.replace(".root",".json.gz"), "wt") as fout:
        fout.write(cset.json(exclude_unset=True))
    

def main():
    """
    steer the script
    """

    # configuration and command line options
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', help='input root file with the correction histograms', default=None, type=str)
    args = parser.parse_args()

    makeHLTCorrecionlib(args.input)

if __name__ == "__main__":
    """
    for execution from another script
    """
    sys.exit(main())
