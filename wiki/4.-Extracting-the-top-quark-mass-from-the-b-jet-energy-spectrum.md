At this point, you are familiar with everything needed to extract the top-quark mass from the b-jet energy spectrum. 
A small group will setup a code to calibrate the b-jet energy peak measured to the expected b-jet energy peak (from which the top-quark mass can be easily extracted) and evaluate the performance of the method, while two other groups will work on systematic uncertainties. 

# Calibration and fit performance

In principle, the top-quark mass could be directly extracted from the b-jet energy peak through

$m_t= E_{b,peak}+\sqrt{m_W^2 - m_b^2 + E^2_{b,peak}}$

(the Particle Data Group giving $m_W = (80.385 \pm 0.015)$ GeV and $m_b = (4.18 \pm 0.03)$ GeV.)

Compute using your fitted peak.

However, after extracting top quark mass from MC, you can see that the mass is not exactly the same as the MC generated mass.

_Question: what can bias this relation?_

_Question: Given the top mass of the nominal sample, 172.5 [GeV](https://twiki.cern.ch/twiki/bin/view/CMS/GeV), what is the predicted value of_ $E_{b,peak}$ ?

## Calibration

Different sources of bias will affect the position of the energy peak. 
Therefore, you need to compute the b-jet energy peak from simulations for several top-quark masses 
in order to precisely know the relation between the b-jet energy peak measured for the set of selection criteria previously defined to the expected b-jet energy peak. 
At 8 TeV, this lead to the following figure:
![image](https://github.com/SWuchterl/LongExerciseTopMass/assets/28142775/7f9859d1-0489-4a42-9c4a-47669a23ebe3)

The nominal samples that have been used to check the data to simulation agreement have been generated for a top-quark mass of 172.5 GeV. 
Samples for the top-pair process are also available for top-quark masses of 169.5 GeV and 175.5 GeV. 
The other samples may be considered as invariable whatever the value of the top-quark mass. 

Simulation Samples
| top quark mass | Sample |$\sigma$ [pb] | 
| ----------- | ----------- | ----------- |
| 169.5 GeV |`/TTto2L2Nu_MT-169p5_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM` <br> `/TTto2L2Nu_MT-169p5_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22NanoAODv12-130X_mcRun3_2022_realistic_v5-v2/NANOAODSIM` | 97.4488 | 
| 175.5 GeV | `/TTto2L2Nu_MT-175p5_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM` <br> `/TTto2L2Nu_MT-175p5_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22NanoAODv12-130X_mcRun3_2022_realistic_v5-v2/NANOAODSIM` | 97.4488 |

For each mass point, you may consider the possibility of completing the
```
fitPeak.py
```
script to:

1. generate pseudo-experiments from the 'bjetenls' distribution with Poissonian fluctuations
2. fit each pseudo-experiments to get a distribution of peak positions
3. take the mean of all the peak positions to complete the calibration curve 

_Question: What is the main point of using pseudo-experiments ?_

A class named `TRandom3()` is implemented in Root so that if you want to compute a `Poissonian` fluctuation for a given value `x`, you can simply do:
```py
random3 = ROOT.TRandom3()
random3.SetSeed(1)
fluctuation = random3.PoissonD(y*math.exp(x))/math.exp(x)
```
Here, `y` is the bin content, and `x` is the bin center for each bin of the `bjetenls` energy distribution.

**Checkpoint: Your macro may look like fitNcalibrate/answers/fitPeak_pseudo.py.**

When you have the values for the 3 different mass points, you can use the
```
peakplotter.py
```
macro to get the relation between the b-jet energy peak measured and the calibrated energy.

Once you have a calibration curve to correct the b-jet energy peak, you may be tempted to convert the b-jet energy peak you got from data into a top-quark mass. **Do not until the end of the exercise!** The Top Mass Working Group has indeed a blinding policy... and you have many other things to do before 😉  


## Fit performance and uncertainty 

The calibration fit introduces a systematic uncertainty due to the uncertainty of the fit parameters that you need to evaluate. To assess the systematic uncertainty of the fit, the covariance between the two fit parameters is required.

You also need to evaluate the performance of the method. One way to do so is to generate pseudo-experiments (for the example, let's say $N_\mathrm{pe}=1000$) from expected b-jet energy distributions for several given top-quark masses (for example, let's consider 171.5 and 173.5 GeV). The number of events in each pseudo-experiment should follow a Poisson distribution whose expected value is the number of events observed in data ($N_\mathrm{evt}$). Each toy is then fitted with the PDF, and the b-jet energy peak value is corrected thanks to the calibration curve.

You get 1,000 calibrated b-jet energy peak measurements for a generated top-quark mass of 171.5 GeV, and the same for 173.5 GeV.

As a closure test, you can have a look at

$\langle E_\mathrm{b,peak}^\mathrm{calibrated} \rangle = f(E_\mathrm{b,peak}^\mathrm{predicted})$

The slope should be 1.

You can also compute the pull distribution

$E_\mathrm{b,peak}^\mathrm{calibrated}-E_\mathrm{b,peak}^\mathrm{predicted}/\Delta E_\mathrm{b,peak}^\mathrm{calibrated}$

for each predicted energy peak position i.e. each generated top-quark mass.

If the mean and width of the pull distributions, as functions of the predicted energy peak position, are flat, around respectively 1 and 0, then the method is unbiased.

It is also interesting to compute the energy peak position uncertainty as function of the predicted peak position. 

# Systematic uncertainties

Different sources of systematic uncertainties affect the top-quark mass measurement. They can be separated in two categories:

Experimental uncertainties:
* Pileup collisions may add jets in reconstructed events. Simulations are reweighed so that the vertex multiplicity is the same in simulations and data. The weights need to be varied within their uncertainty.
* The b-tagging, trigger and lepton selection efficiencies may differ between data and simulations. Scale factors are applied to simulation. They need to be varied within their uncertainty. 
* The aim is to select top-pair events. All the other events that pass the selection criteria are considered as background. Background processes are normalized at their cross-sections, which are known with a limited accuracy. Thus, background normalizations need to be varied (by 25% for single top, 100% for W+jets, and 50% for Drell-Yan and diboson). 
* The jet energy resolution is corrected in simulations to match the one observed in data (already in the analysis code of `runBJetEnergyPeak`). It also has to be varied within its uncertainty.
* The jet energy scale (JES) is potentially different for data and simulations. There are several kinds of jet energy corrections (JEC), that must be varied within their uncertainty. 

Theoretical uncertainties:
* The parton distribution functions need to be varied, this can be done based on weights stored in NanoAOD.
* The renormalization and factorization scale (Q) needs to be varied by factors 1/2 and 2, this can be done based on weights stored in NanoAOD.
* It has been observed in several analyses at 8 and 13 TeV that the top-quark $p_T$ is not perfectly reproduced in simulations. Simulations are not corrected for top-quark measurements but they need to be [reweighed](https://twiki.cern.ch/twiki/bin/viewauth/CMS/TopPtReweighting) so that the top-quark $p_T$ distribution matches the one observed in data in order to consider this mis-modeling as a source of systematic uncertainty. Although this method should be robust to the top-quark boost, the measurement is actually impacted by the top-quark $p_T$ uncertainty.
* The underlying event can not be described by perturbative QCD. Generators are tuned to data with a limited accuracy. To take this into account, you need to compare samples with different tunes. 

_Question: Which sources do you expect to be dominant ? Why ?_

In the following figure, you can see the effect of the main sources of systematic uncertainty on the b-jet energy distribution in log scale: 
![image](https://github.com/SWuchterl/LongExerciseTopMass/assets/28142775/fe677b95-9281-411a-89f7-8daac437a22b)

For each source of systematic uncertainty, uncertainties in the b-jet energy have to be added to (subtracted from) the nominal value to get two bjetenls distributions. Once two distributions are obtained, fitting can be performed as usual. The calibrated b-jet peak values will give upper and lower bound of top quark mass. Taking a difference of the upper and lower bound of the top mass and dividing it by 2, will give you an estimate on the top mass uncertainty due to the specific systematic uncertainty. This has to be repeated many times for the different sources of systematic uncertainties. If the uncertainties are not correlated to one another, they can be added in quadrature to one another to get a single uncertainty value since they are not correlated to one another. 

Try to evaluate as much systematic uncertainties as possible, focusing as a priority on the most relevant ones. Several simulation samples are available to help you: 

Simulation samples:
| Uncertainty source | Sample | $\sigma$ [pb] | 
| ----------- | ----------- | ----------- | 
| ME/PS matching | `/TTto2L2Nu_Hdamp-{158,418}_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22NanoAODv12-130X_mcRun3_2022_realistic_v5-v2/NANOAODSIM` <br> `/TTto2L2Nu_Hdamp-{158,418}_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM` | 97.4488  | 
| Uncerlying event tune | `/TTto2L2Nu_TuneCP5{Up,Down}_13p6TeV_powheg-pythia8/Run3Summer22NanoAODv12-130X_mcRun3_2022_realistic_v5-v2/NANOAODSIM` <br> `/TTto2L2Nu_TuneCP5{Up,Down}_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM` | 97.4488  |
| Early resonance decays (ERD) | `/TTto2L2Nu_TuneCP5_ERDOn_13p6TeV_powheg-pythia8/Run3Summer22NanoAODv12-130X_mcRun3_2022_realistic_v5-v2/NANOAODSIM` <br> `/TTto2L2Nu_TuneCP5_ERDOn_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM`| 97.4488  |
| Color reconnection | ... | 97.4488 |

A skeleton is available in the `analyzeNplot` folder. Let's open
``
systBJetEnergyPeak.py
``
The `main()` is at the end, as usual, but the interesting part is the runBJetEnergyPeak function at line 14. The histograms for the different systematic variations are defined and then filled in a loop on the events.

To run this code:
```sh
python systBJetEnergyPeak.py -o systematics
```
