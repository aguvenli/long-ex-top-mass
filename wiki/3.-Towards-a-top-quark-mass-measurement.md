We will first get familiar with the format of the input files, and explore one way to read them and apply a selection to the events that they contain. 
The goal is to obtain the b-jet energy spectrum for top-pair events decaying in the eμ channel.

#  Analyzing the inputs 

Let's inspect an input file from Python.
First, have a look at the list of input files. As you can see, for simulations, we have not only top-pair processes but also Drell-Yan, diboson, and others. The reason for that will become clear soon.
The files are copied to the CERN eos area and we use [Xrootd](https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookXrootdService) to find them (never use the eos mount directly):
```sh
xrdfs root://eoscms.cern.ch/ ls -l /eos/cms/store/
xrdfs root://eoscms.cern.ch/ ls -l /eos/cms/store/data/
...
xrdfs root://eoscms.cern.ch/ ls -l /eos/cms/store/data/Run2022C/MuonEG/NANOAOD/22Sep2023-v1/2520000
```

Now, open one of the data files:
```py
python
from ROOT import TFile
inF=TFile.Open("root://eoscms.cern.ch//eos/cms/store/data/Run2022C/MuonEG/NANOAOD/22Sep2023-v1/2520000/6c474b9d-4e5c-44ea-b95a-45d21840ea4b.root")
inF.ls()
```
You should see the following output:
```
TNetXNGFile**		root://eoscms.cern.ch//eos/cms/store/data/Run2022C/MuonEG/NANOAOD/22Sep2023-v1/2520000/6c474b9d-4e5c-44ea-b95a-45d21840ea4b.root	
 TNetXNGFile*		root://eoscms.cern.ch//eos/cms/store/data/Run2022C/MuonEG/NANOAOD/22Sep2023-v1/2520000/6c474b9d-4e5c-44ea-b95a-45d21840ea4b.root	
  KEY: TObjString	tag;1	Collectable string class
  KEY: TTree	Events;1	Events
  KEY: TTree	LuminosityBlocks;1	LuminosityBlocks
  KEY: TTree	Runs;1	Runs
  KEY: TTree	MetaData;1	Job metadata
  KEY: TTree	ParameterSets;1	Parameter sets
```

The file contains one tree with per-event information, named Events. The contents of this tree can be listed as follows:
```py
t=inF.Get("Events")
t.Print()
```

The output is long and skipped for brevity. The meaning of the variables can be found under [this](https://cms-nanoaod-integration.web.cern.ch/autoDoc/) page for [Data](https://cms-nanoaod-integration.web.cern.ch/autoDoc/NanoAODv12/2022/2023/doc_EGamma_Run2022F-22Sep2023-v1.html) and for [MC](https://cms-nanoaod-integration.web.cern.ch/autoDoc/NanoAODv12/2022/2023/doc_DYJetsToLL_M-50_TuneCP5_13p6TeV-madgraphMLM-pythia8_Run3Summer22NanoAODv12-130X_mcRun3_2022_realistic_v5-v2.html)

A useful tool to have a quick glance at the contents of an ntuple is TTree::Scan, for example: 
```py
t.Scan("PV_npvsGood:nJet")
```
to get:
```
************************************
*    Row   * PV_npvsGo *      nJet *
************************************
*        0 *        35 *         3 *
*        1 *        27 *         4 *
*        2 *        34 *         3 *
*        3 *        20 *         3 *
*        4 *        28 *         2 *
*        5 *        36 *         3 *
*        6 *        33 *         4 *
...
Type <CR> to continue or q to quit ==>
```

_Question: Think first how top quark pairs can decay and what final states are possible. Then, if you want to study dileptonic electron-muon candidate events alone, what conditions (cuts) would you apply? Which other processes would then be the main background (think of what other physics processes can have a similar final state)?_

Let's start by plotting the transverse momentum of jets and b-tagged jets. 
```py
t.Draw("Jet_pt")
t.Draw("Jet_pt", "Jet_btagRobustParTAK4B>0.4319", "e1same")
```

Let's now turn to MC: it's interesting to take a look at the same quantities as above, as expected from simulation. For that purpose, open a file with simulated top-pair events, and plot the generated transverse momentum of the jets.
```py
mcinF=TFile.Open("/eos/cms/store/mc/Run3Summer22NanoAODv12/TTto2L2Nu_TuneCP5_13p6TeV_powheg-pythia8/NANOAODSIM/130X_mcRun3_2022_realistic_v5-v2/2520000/66b834d6-61f7-4109-b5ae-54a150d4814b.root")
mct=mcinF.Get("Events")
mct.Draw("GenJet_pt")
mct.Draw("GenJet_pt", "abs(GenJet_hadronFlavour)==5", "e1same")
```

Let's plot the expected resolution for b-jet $p_\mathrm{T}$. We define resolution as $\frac{p_\mathrm{T}^\mathrm{rec}-p_\mathrm{T}^\mathrm{gen}}{p_\mathrm{T}^\mathrm{gen}}$ where $p_\mathrm{T}^\mathrm{rec}$ ($p_\mathrm{T}^\mathrm{gen}$) is the reconstructed (generated) transverse momentum of a jet.
```py
mct.Draw("(Jet_pt - GenJet_pt[Jet_genJetIdx])/GenJet_pt[Jet_genJetIdx]","Jet_genJetIdx!=-1 && GenJet_hadronFlavour[Jet_genJetIdx]==5")
```
What about the leptons? Let us apply some identification and isolation criteria on the reconstructed electron objects, and plot the resulting $p_\mathrm{T}$ spectrum. Then, we can compare that with the same quantity, but after requiring also that the electrons are genuine electrons.
```py
mct.Draw("Electron_pt","Electron_cutBased>=2 && Electron_pt > 20 && Electron_pt < 200")
mct.Draw("Electron_pt","Electron_cutBased>=2 && Electron_pt > 20 && Electron_pt < 200 && Electron_genPartFlav==1", "e1same")
```
Let's do the same for muons:
```py
mct.Draw("Muon_pt","Muon_looseId && Muon_pt > 20 && Muon_pt < 200")
mct.Draw("Muon_pt","Muon_looseId && Muon_pt > 20 && Muon_pt < 200 && Muon_genPartFlav==1", "e1same")
```
_Question: What objects could be reconstructed as electrons but are not genuine electrons?_

_Question: What is the reason for the $p_T$ requirement in the pre-selection step?_

# Event selection

Now that you are familiar with the input files, let's apply a tighter event selection (such as requirements on the jet and b-tagged jet multiplicities, tighter cut on the b-jet tag, etc.) and produce the b-jet energy distribution. The standard operating point values for the different requirements can be found [here.](https://btv-wiki.docs.cern.ch/ScaleFactors/Run3Summer22/) A skeleton is available in the analyzeNplot folder, for you to use as starting point. Let's open
```
runBJetEnergyPeak.py
```

The `main()` is of course at the end but the interesting part happens actually in the `runBJetEnergyPeak()` function. Events are analyzed using ROOT [RDataFrame](https://root.cern/doc/master/classROOT_1_1RDataFrame.html).

_Question: What are the current selection criteria on the electrons, muons, jets, and events?_

You can run this code as follows:
```sh
python runBJetEnergyPeak.py -o test --era 2022 --maxFiles 1 --selectSamples TTto2L2Nu --nThreads 0
```
where `-o` defines the name of the output folder (`test`, in this case). The argument `--era` defines the data taking period - the 2022preEE period in this case (which is also the default). The `--maxFiles` argument is important for testing the code: use 1 to process only one file per sample, which is much faster. 
The `--selectSamples` is also useful for testing or running over a few samples only, in the example only the signal sample `TTto2L2Nu` is ran.
The `--nThreads` argument defines the number of threads to be used in multithreading, for 0 (default), all threads on the machine are used. 
Other command line arguments are explained later.

The information for each sample is given in the file resources.py: the file paths, the cross sections, luminosities, and finally the colors and labels.

It takes several minutes to run over all the input files.

In the meanwhile, you can start improving the python code. Remember, before running over all files first run with `--maxFiles 1` to ensure the code is working.

The histograms are defined further down in the `runBJetEnergyPeak()` function:
```py
    histos.extend([
        rdf.Histo1D(('nvtx',';Vertex multiplicity; Events',100,0,100), "PV_npvsGood", "weight"),
        rdf.Define("nGoodJets", "Sum(TightJet_good)") \
            .Histo1D(('njets',';jet multiplicity; Events',7,1,8), 'nGoodJets', "weight"),
        rdf.Define("nGoodTaggedJets", "Sum(GoodJet_tagged)") \
    ...
    ])
```
_Question: Which other distributions could be interesting?_

Add the necessary lines to include additional histograms. As you can notice, not only the b-jet energy distribution is computed ('bjeten'), but also the energy distribution on a logarithmic scale ('bjetenls'). As a matter of fact, it is easier to find the peak of the $\log(E_\mathrm{b})$ distribution rather than of the $E_\mathrm{b}$ spectrum, as the $\log(E_\mathrm{b})$ distribution is symmetric around its peak. In order to not distort the shape, the $\log(E_\mathrm{b})$ distribution need to be reweighed by $1/E$:

$\frac{\mathrm{d}N}{\mathrm{d}E_\mathrm{b}}=\frac{1}{E_\mathrm{b}}\frac{\mathrm{d}N}{\mathrm{d}\log E_\mathrm{b}}$

**Checkpoint: Your macro may look like analyzeNplot/answers/runBJetEnergyPeak.py.**

To see the data to simulation agreement, you can produce plots by running
```sh
python plotter.py -i test
```
Under `test/plots`, you'll find a file called `plotter.root`, containing the histograms with the distributions normalized by the integrated luminosity, together with `png` and `pdf` versions of the plots.

The number of events for each process can be obtained as follow
```sh
python getNumberOfEvents.py -i test -o table
```
The output is stored both in the `pdf` and `LaTeX` formats, in the `table` subfolder.

Once you have a first set of histograms, think twice about the selection criteria, and adapt the code accordingly.

# Corrections

An important ingredient of any CMS analysis are the corrections for data and simulation. 
In fact, you may have noticed the `weight` column, which is a multiplication of several weights. 
For example, one of them is `weight_pileup`. In simulation, the events of interest are overlaid with a random number of minimum bias events in order to recreate a given pileup scenario. To match the simulated pileup scenario to the one observed in the data, the events have to be re-weighted. These weights are often also called scale factors. Besides, trigger, muon, and electron trigger selection efficiencies are not exactly the same in simulation and in data. This is also taken into account through weights. 

In the analysis, the [correctionlib](https://cms-nanoaod.github.io/correctionlib/) library is used, which provides sets of corrections which are centrally computed in CMS. 
The corrections first have to be loaded and declared in the `declareCorrectors()` function before they can be accessed.
Information on centrally provided corrections can be found [here](https://cms-nanoaod-integration.web.cern.ch/commonJSONSFs/). 
For example for the electron scale factors [here](https://cms-nanoaod-integration.web.cern.ch/commonJSONSFs/summaries/EGM_2022_Summer22_electron.html) you can find the information on the name of the corrections (`Electron-ID-SF`), the inputs (`year`, `ValType`, `ValType`, `eta`, `pt`) and output (`weight`) with the corresponding required type and a short description.

The scale factors for the muon isolation is still missing, can you add it? 

Corrections for trigger efficiencies for the dataset we are using are not yet centrally provdided. Luckily, another analysis that uses the same data set, trigger, and object selection has already been performed. This is the first Run 3 measurement of tW [TOP-23-008](https://cds.cern.ch/record/2893766?ln=en). 
They kindly provided us with root histograms containing their custom trigger scale factors. 
To convert them into the format needed for correctionlib you can run
```sh
python makeHLTCorrectionlib.py -i data/triggerSFs_CD.root
```
and 
```sh
python makeHLTCorrectionlib.py -i data/triggerSFs_EFG.root
```
and you should find the needed files `triggerSFs_CD.json.gz` and `triggerSFs_EFG.json.gz` in the data folder. 
Now you can load and declare the corrections with the trigger scale factors first in the `declareCorrectors()` function, and then access them and apply them as weights in the `runBJetEnergyPeak()` function. 

Another important correction is for the b tagging. For this kind of correction, the b tagging efficiencies with your particular selection are needed, so you will need to compute them. 
Once you finalized your selection, you can run the analysis with 
```sh
python runBJetEnergyPeak.py -o forBtagEfficiencies --measureBtagEfficiencies
```
to produce the required histograms. Then you can run 
```sh
python measureBtagEfficiency.py --inDir forBtagEfficiencies --outDir data
```
to compute the efficiencies. After that, you can run the analysis again
```sh
python runBJetEnergyPeak.py -o nominal
```
Everything should be set up such that the b-tag reweighting is applied correctly.

Other corrections are related to the jet energy and pT resolution corrections. 
For the jet energy correction the four vectors of the jets are adjusted in data and MC. For the pT resolution, the MC is corrected. 
Since the event selection relies on the number of jets that have a minimum pT, the analysis selection must be re evaluated with the varied four vectors.

_Question: Which corrections have the largest impact?_


# Fitting the b-jet energy spectrum 

Next step is to fit the b-jet energy (`bjetenls`) peak. A skeleton is available in the `fitNcalibrate` folder. Let's open
```
fitPeak.py
```
You can run this script either
```sh
python fitPeak.py -i "../analyzeNplot/nominal"
```
for MC (signal+background, normalized at their cross-sections, are fitted together) or
```sh
python fitPeak.py -i "../analyzeNplot/nominal" -d
```
for data. The argument following -i is the folder in which the plotter.root file has been previously produced. In this example of usage, the output is stored under `nominal/fit_MC.pdf` for MC and `nominal/fit_Data.pdf` for data.

Let's dig into the code. The `main()` function is, as usual, at the end, but it is not the most interesting part. Let's go to the top. As you can see, the `gPeak()` function performs a fit of the b-jet energy spectrum in logarithmic scale, using a gaussian PDF between 3.6 and 4.8 GeV. The parameter you are interested in is the mean $\mu$ of the gaussian. The b-jet energy peak is indeed $\langle E_{b} \rangle = \exp{(\mu)} \pm \Delta\mu\cdot\exp{(\mu)} $

Play a little bit with the mean and width limit values
```py
## Set normalization
fitfunc.SetParameter(0, h.Integral())
fitfunc.SetParLimits(0, 0.1*h.Integral(), 2.5*h.Integral())
## Set gaussian mean starting value and limits
fitfunc.SetParameter(1, 4.2)
fitfunc.SetParLimits(1, 4., 4.4)
## Set gaussian width starting value and limits
fitfunc.SetParameter(2, 0.65)
fitfunc.SetParLimits(2, 0.35, 0.95)
```
the range fit
```ini
## Set limits
minToFit = 3.6
maxToFit = 4.8
```
and, as there is no physical motivation behind it, even the function
```py
def myFitFunc(x=None,par=None):
    return par[0]*ROOT.TMath.Gaus(x[0],par[1],par[2],ROOT.kFALSE)
```
(you could try a polynomial function ?). 