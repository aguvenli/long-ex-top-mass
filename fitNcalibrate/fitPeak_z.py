import math, ROOT, json, argparse
import os, sys

import tdrstyle

def myFitFunc(x=None,par=None):
    return par[0]*ROOT.TMath.Gaus(x[0],par[1],par[2],False)

def gPeak(h=None,inDir=None,isData=None,lumi=None):

    # Set the stats off 
    ROOT.gStyle.SetOptStat(0)
    ROOT.gStyle.SetOptTitle(0)
    ROOT.gStyle.SetTickLength(0.03)

    # Get the log(E) histogram 
    hFit = h.Clone()
    hFit.SetMarkerStyle(8)
    hFit.GetYaxis().SetTitleSize(0.062)
    hFit.GetYaxis().SetLabelSize(0.062)
    hFit.GetYaxis().SetTitleOffset(0.62)
    hFit.GetYaxis().SetTitle("1/E dN_{b jets}/dlog(E)")
    hFit.GetXaxis().SetLabelOffset(1)
    hFit.GetXaxis().SetTitle("log(E)")
    hFit.SetLineColor(ROOT.kBlack)
    hFit.SetMarkerColor(ROOT.kBlack)

    # Define the fit function and parameters
    ## Set limits
    minToFit = 3.6
    maxToFit = 4.8
    ## Set the function
    fitfunc = ROOT.TF1("Gaussian fit", myFitFunc, minToFit, maxToFit, 3)
    ## Set normalization
    fitfunc.SetParameter(2, h.Integral())
    fitfunc.SetParLimits(2, 0.1*h.Integral(), 2.5*h.Integral())
    ## Set gaussian mean starting value and limits
    fitfunc.SetParameter(1, 4.2)
    fitfunc.SetParLimits(1, 4., 4.4)
    ## Set gaussian width starting value and limits
    fitfunc.SetParameter(2, 0.65)
    fitfunc.SetParLimits(2, 0.35, 0.95)
    ## Some cosmetics
    fitfunc.SetLineColor(kRed)
    fitfunc.SetLineWidth(3)
    fitfunc.SetLineStyle(1)

    # Do the fit
    hFit.Fit("Gaussian fit","EMQ", "", minToFit, maxToFit) 
    # "E" stands for Minos, "M" for improving fit results
    # cf. ftp://root.cern.ch/root/doc/5FittingHistograms.pdf    

    # Get Fit Parameters
    mean = fitfunc.GetParameter(1)
    meanErr = fitfunc.GetParError(1)
    sigma = fitfunc.GetParameter(2)
    sigmaErr = fitfunc.GetParError(2)
    chi2 = fitfunc.GetChisquare()
    NDF = fitfunc.GetNDF()
    chi2ndf = chi2/NDF
    # Calculate the uncalibrated Energy peak position and its uncertainty
    Ereco = math.exp(mean)
    Err = abs(Ereco*meanErr)

    #all done here )
    return Ereco,Err

def plotter(h=None,name=None):
    c1 = ROOT.TCanvas("c1","")
    c1.cd()
    tdrstyle.setTDRStyle()
    gROOT.ForceStyle()
    gROOT.Reset()
    h.UseCurrentStyle()
    h.Fit("gaus")
    h.Draw()

    label1 = ROOT.TLatex()
    label1.SetNDC()
    label1.SetTextFont(60)
    label1.SetTextSize(0.07)
    label1.SetTextAlign(31)
    label1.DrawLatex(0.32, 0.92, "CMS DAS")
    label2 = ROOT.TLatex()
    label2.SetNDC()
    label2.SetTextFont(42)
    label2.SetTextSize(0.06)
    label2.SetTextAlign(11)
    label2.DrawLatex(0.33, 0.92, "#it{Simulation}")

    c1.Update()
    stats = c1.GetPrimitive("stats")
    stats.__class__ = ROOT.TPaveStats
    stats.SetY1NDC(0.6)
    stats.SetY2NDC(0.9)
    stats.SetX1NDC(0.6)
    stats.SetX2NDC(0.9)
    c1.RedrawAxis()
    c1.Update()

    c1.SaveAs(name)
    c1.Close()


def main():
    fOut=ROOT.TFile.Open("cuizhipeng.root",'RECREATE')
   # for key in histos: histos[key].Write()

    f = TFile("cuizhipeng.root", "recreate")
    gROOT.SetBatch(True)
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--isData', action='store_true')
    parser.add_argument('-l', '--lumi',  help='Luminosity value in /pb for plotting', default=1, type=float)
    parser.add_argument('-i', '--inDir',  help='input directory', default='nominal', type=str)
    args = parser.parse_args()
    
    # Open the root file
    fiName = args.inDir+"/plots/plotter.root"
    print(f"... processing {fiName}")
    if not os.path.isfile(fiName):
        raise IOError("Help, file doesn't exist")
    inF = ROOT.TFile(fiName, "read")

    inD = inF.Get("bjetenls")

    #Get the histogram
    if args.isData is True:
        hName = "bjetenls"
        histo = inD.Get(str(hName))
        histo.SetDirectory(0)
    else:
        histo = None
        #iterate over all processes
        for tkey in inD.GetListOfKeys():
            key=tkey.GetName()
            hist=inD.Get(key)
            if not hist.InheritsFrom('TH1'): 
                continue
            name = hist.GetTitle()
            print(name)
            if name == "Data":
                continue
            hName = f"bjetenls_{name}"
            if histo is None:
                histo = hist
                histo.SetDirectory(0)
            else:
                histo.Add(inD.Get(hName).Clone())

    # Create the output directory
    if not os.path.isdir(args.inDir):
        os.mkdir(args.inDir)

    #Generate pseudo-exp
    r3 = TRandom3()
    r3.SetSeed(34252)
    Npe = 1500
    #heb = ROOT.TH1F("heb", "", 50,61,68) # 169v5
    #heb = ROOT.TH1F("heb", "", 50,63,68) # 172v5
    heb = ROOT.TH1F("heb", "", 50,64,69) # 175v5

    #hde = ROOT.TH1F("hde", "", 30,0.09,0.2) # 169v5
    #hde = ROOT.TH1F("hde", "", 30,0,0.1) # 172v5
    hde = ROOT.TH1F("hde", "", 30,0.08,0.3) # 175v5

    hpull = ROOT.TH1F("hpull", "",100,-150,-80)
    hpullcal = ROOT.TH1F("hpullcal", "",100,-70,0)

    #pred = 65.740 #169v5
    #pred = 67.57 #172v5
    pred = 69.39 #175v5

    for i in range(0,Npe):
        hpe = histo.Clone()
        for ibin in range(0,histo.GetNbinsX()):
            y = histo.GetBinContent(ibin)
            x = histo.GetXaxis().GetBinCenter(ibin)
            fluct = r3.PoissonD(y*math.exp(x))/math.exp(x)
            hpe.SetBinContent(ibin,fluct)
            err = math.sqrt(fluct)/math.exp(x)
            hpe.SetBinError(ibin,err)
        # Calculate the energy peak position in the big MC sample
        Eb,DEb = gPeak(h=hpe,inDir=args.inDir,isData=args.isData,lumi=args.lumi)
        heb.Fill(Eb)
        hde.Fill(DEb)
        pull=(Eb-pred)/DEb
        hpull.Fill(pull)

        Ebcal=(Eb-29.6)/0.5312
        DEbcal=DEb/0.5312
        #print("Eb:", Eb, "  DEb:", DEb, "  Pull:", pull, "Delta:", abs(Eb-pred))
        pullcal=(Ebcal-pred)/DEbcal
        hpullcal.Fill(pullcal)
      #  print(Eb)
    heb.Write()
    hde.Write()
    hpull.Write()
    hpullcal.Write()
    plotter(heb,"Eb.pdf")
    plotter(hde,"Deb.pdf")
    plotter(hpull,"Pull.pdf")
    plotter(hpullcal,"Pull_corr.pdf")

    res.Close()
   # f.Write()
    fOut.Close()
if __name__ == "__main__":
    sys.exit(main())


