import math, ROOT, json, argparse
import os, sys

def myFitFunc(x=None,par=None):
    return par[0]*ROOT.TMath.Gaus(x[0],par[1],par[2],False)

def gPeak(h=None,inDir=None,isData=None,lumi=None):

    # Set the stats off
    ROOT.gStyle.SetOptStat(0)
    ROOT.gStyle.SetOptTitle(0)
    ROOT.gStyle.SetTickLength(0.03)

    # Get the log(E) histogram
    hFit = h.Clone()
    hFit.SetMarkerStyle(8)
    hFit.GetYaxis().SetTitleSize(0.062)
    hFit.GetYaxis().SetLabelSize(0.062)
    hFit.GetYaxis().SetTitleOffset(0.62)
    hFit.GetYaxis().SetTitle("1/E dN_{b jets}/dlog(E)")
    hFit.GetXaxis().SetLabelOffset(1)
    hFit.GetXaxis().SetTitle("log(E)")
    hFit.SetLineColor(ROOT.kBlack)
    hFit.SetMarkerColor(ROOT.kBlack)

    # Define the fit function and parameters
    ## Set limits
    minToFit = 3.6
    maxToFit = 4.8
    ## Set the function
    fitfunc = ROOT.TF1("Gaussian fit", myFitFunc, minToFit, maxToFit, 3)
    ## Set normalization
    fitfunc.SetParameter(0, h.Integral())
    fitfunc.SetParLimits(0, 0.1*h.Integral(), 2.5*h.Integral())
    ## Set gaussian mean starting value and limits
    fitfunc.SetParameter(1, 4.2)
    fitfunc.SetParLimits(1, 4., 4.4)
    ## Set gaussian width starting value and limits
    fitfunc.SetParameter(2, 0.65)
    fitfunc.SetParLimits(2, 0.35, 0.95)
    ## Some cosmetics
    fitfunc.SetLineColor(ROOT.kBlue)
    fitfunc.SetLineWidth(3)
    fitfunc.SetLineStyle(1)

    # Do the fit
    hFit.Fit("Gaussian fit","EM", "", minToFit, maxToFit)
    # "E" stands for Minos, "M" for improving fit results
    # cf. ftp://root.cern.ch/root/doc/5FittingHistograms.pdf

    # Get Fit Parameters
    mean = fitfunc.GetParameter(1)
    meanErr = fitfunc.GetParError(1)
    sigma = fitfunc.GetParameter(2)
    sigmaErr = fitfunc.GetParError(2)
    chi2 = fitfunc.GetChisquare()
    NDF = fitfunc.GetNDF()
    chi2ndf = chi2/NDF
    # Calculate the uncalibrated Energy peak position and its uncertainty
    Ereco = math.exp(mean)
    Err = abs(Ereco*meanErr)

    # Make a pull distribution
    hPull = h.Clone("Pull")
    for ibin in range(1, hFit.GetNbinsX()+1):
        if hFit.GetBinCenter(ibin) > minToFit and hFit.GetBinCenter(ibin) <= maxToFit:
            binCont = hFit.GetBinContent(ibin)
            binErr = hFit.GetBinError(ibin)
            valIntegral = fitfunc.Eval(hFit.GetBinCenter(ibin))
            if binErr !=0:
              pull = (binCont-valIntegral)/binErr
              hPull.SetBinContent(ibin, pull)
              hPull.SetBinError(ibin, 1)
        else:
            hPull.SetBinContent(ibin, 0.)
            hPull.SetBinError(ibin, 0.)
    hPull.SetMarkerStyle(8)
    hPull.GetYaxis().SetNdivisions(504)
    hPull.GetYaxis().SetTitleSize(0.140)
    hPull.GetYaxis().SetLabelSize(0.140)
    hPull.GetYaxis().SetTitleOffset(0.27)
    hPull.GetYaxis().SetTitle("#frac{Data-Fit}{Uncertainty}")
    hPull.GetXaxis().SetTitleSize(0.160)
    hPull.GetXaxis().SetLabelSize(0.150)
    hPull.GetXaxis().SetTitleOffset(0.8)
    hPull.GetXaxis().SetTitle("log(E)")
    hPull.SetLineColor(ROOT.kBlack)
    hPull.SetMarkerColor(ROOT.kBlack)

    # Plot the result
    ## Create a canvas with two pads for plotting your histograms
    c = ROOT.TCanvas('c','c')
    p1 = ROOT.TPad('p1','p1',0.,0.3,1.0,1.0)
    p1.SetBorderMode(0)
    p1.SetBorderSize(2)
    p1.SetTickx(1)
    p1.SetTicky(1)
    p1.SetTopMargin(0.13)
    p1.SetBottomMargin(0.02)
    p1.Draw()
    p2 = ROOT.TPad('p2','p2',0.,0.,1.0,0.3)
    p2.SetGridy()
    p2.SetBorderMode(0)
    p2.SetBorderSize(2)
    p2.SetTickx(1)
    p2.SetTicky(1)
    p2.SetTopMargin(0.05)
    p2.SetBottomMargin(0.3)
    p2.Draw()
    ## Draw in the pad of the fit
    p1.cd()
    hFit.GetXaxis().SetRangeUser(minToFit,maxToFit)
    hFit.Draw()
    ##Create some labels about the statistics
    caption1 = ROOT.TLatex()
    caption1.SetTextSize(0.045)
    caption1.SetTextFont(42)
    caption1.SetNDC()
    caption1.DrawLatex(0.75,0.8,'Fit Results')
    caption1.DrawLatex(0.73,0.76,'#mu = %4.2f #pm %4.2f'%(mean,meanErr))
    caption1.DrawLatex(0.73,0.72,'#sigma = %4.2f #pm %4.2f'%(sigma,sigmaErr))
    caption1.DrawLatex(0.74,0.67,'#chi^{2}/ndf = %4.2f'%(chi2ndf))
    caption2 = ROOT.TLatex()
    caption2.SetTextSize(0.05)
    caption2.SetTextFont(42)
    caption2.SetNDC()
    caption2.DrawLatex(0.35,0.44,'Uncalibrated Measurement')
    caption2.DrawLatex(0.35,0.39,'<E_{b}> = (%4.2f #pm %4.2f) GeV'%(Ereco,Err))
    ## CMS labels
    label1 = ROOT.TLatex()
    label1.SetNDC()
    label1.SetTextFont(60)
    label1.SetTextSize(0.09)
    label1.SetTextAlign(31)
    label1.DrawLatex(0.19, 0.9, "CMS")
    label2 = ROOT.TLatex()
    label2.SetNDC()
    label2.SetTextFont(42)
    label2.SetTextSize(0.0765)
    label2.SetTextAlign(11)
    if isData is True:
        label2.DrawLatex(0.2, 0.9, "#it{Preliminary}")
    else:
        label2.DrawLatex(0.2, 0.9, "#it{Simulation preliminary}")
    label3 = ROOT.TLatex()
    label3.SetNDC()
    label3.SetTextFont(42)
    label3.SetTextSize(0.0765)
    label3.SetTextAlign(31)
    label3.DrawLatex(0.90, 0.9, "%d pb^{-1} (13 TeV)" % lumi)
    ## Edit the pad for the pull
    p2.cd()
    hPull.GetXaxis().SetRangeUser(minToFit,maxToFit)
    hPull.Draw("e")

    #save and delete
    sName = inDir+"/fit_"
    if isData is True:
        sName = sName+"Data"
    else:
        sName = sName+"MC"
    c.SaveAs(sName+".pdf")
    c.SaveAs(sName+".png")
    del c
    fitfunc.IsA().Destructor(fitfunc)
    del caption1,caption2

    #all done here ;)
    return Ereco,Err

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--isData', action='store_true')
    parser.add_argument('-l', '--lumi',  help='Luminosity value in /pb for plotting', default=1, type=float)
    parser.add_argument('-i', '--inDir',  help='input directory', default='nominal', type=str)
    args = parser.parse_args()

    # Open the root file
    fiName = args.inDir+"/plots/plotter.root"
    print(f"... processing {fiName}")
    if not os.path.isfile(fiName):
        raise IOError("Help, file doesn't exist")
    inF = ROOT.TFile(fiName, "read")

    inD = inF.Get("bjetenls")

    #Get the histogram
    if args.isData is True:
        hName = "bjetenls"
        histo = inD.Get(str(hName))
        histo.SetDirectory(0)
    else:
        histo = None
        #iterate over all processes
        for tkey in inD.GetListOfKeys():
            key=tkey.GetName()
            hist=inD.Get(key)
            if not hist.InheritsFrom('TH1'): 
                continue
            name = hist.GetTitle()
            print(name)
            if name == "Data":
                continue
            hName = f"bjetenls_{name}"
            if histo is None:
                histo = hist
                histo.SetDirectory(0)
            else:
                histo.Add(inD.Get(hName).Clone())

    # Create the output directory
    if not os.path.isdir(args.inDir):
        os.mkdir(args.inDir)

    # Calculate the energy peak position in the big MC sample
    Eb,DEb = gPeak(h=histo,inDir=args.inDir,isData=args.isData,lumi=args.lumi)
    print("<E_{b}> = (%3.2f #pm %3.2f) GeV" % (Eb,DEb))

    inF.Close()

if __name__ == "__main__":
    sys.exit(main())
